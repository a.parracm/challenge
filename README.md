## Challenge

Alejandro Parra Jimenez

## dependencies

Composer
Laravel


## Instructions

1.- Clone the repository in your local machine

2.- run "composer install"

3.- Create the file .env and set the DB information

4.- run php artisan migrate

5.- Go to "https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx" and download the zip codes csv files

6.- Import the information (zip csv file) to the zips table in your database

7.- run 

php artisan config:cache
php artisan route:cache
php artisan view:cache
php artisan optimize
php artisan serve


8.- open your web browser and navigate to "http://127.0.0.1:8000/api/zip-codes/01210", you can set the zip code using something like "http://127.0.0.1:8000/api/zip-codes/[your zip code]"

## Solution

I created a migration for the zip codes  with an index in the field 
The providers unnecessaries was been commented in "config\app.php"
I used cache in order to increse the performance

I test a lot of solutions with eloquent and sql raw, using map, for and foreach as you can see in "routes\api.php" and "app\Http\Controllers\ZipController.php". 

The cp with more children "settlements" items are:
'85203'
'33970'
'85218'
'33123'
'33196'
'33637'
'83303'
'83300'
'85213'
'33195'
'33188'
I used these zip codes in my benchmark.

I got it with "SELECT d_codigo, count(d_codigo) as items FROM zips group by (d_codigo) having count(d_codigo) order by items DESC;"


The solution with best performance was

1.- Use \DB::select to get the information from the DB
2.- Use for stament to generate the "settlements" field

Note: I use a shared hosting that is not powerful, when i tested in local i got response faster than 30 ms


## TODO
Normalize the db

## Tested with
https://user.dotcom-monitor.com/
