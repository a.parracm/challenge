<?php

namespace App\Http\Controllers;

use App\Models\Zip;
use Illuminate\Http\Request;

class ZipController extends Controller
{
    /**
     * Show the /api/zip-codes/{zip code}.
     *
     * @param  String(5)  $zip
     */

    //Most Faster solution with sql raw and for to generate the settlements field
    public function show_sql_raw_for($zip)
    {
        
        //is valid zip code?
        if(isset($zip[5]))//this is faster than if(strlen($zip)!=5)
        {
            return response('Not found', 404);
        }


        /*This one is slower than the next one
        $zips = \DB::select("
SELECT  
		d_asenta,
        d_tipo_asenta,
        D_mnpio,
        d_estado,
        d_ciudad,
        d_zona
FROM 
	zips 
    WHERE  MATCH(d_codigo) AGAINST (? IN BOOLEAN MODE);        
        ", [$zip]); // Response in production server is under 300 ms when use the zip code with most children
        */


$zips = \DB::select("SELECT 
        d_asenta,
        d_tipo_asenta,
        D_mnpio,
        d_estado,
        d_ciudad,
        d_zona
        FROM 
            zips 
        WHERE 
            d_codigo=? ", [$zip]); // Response in production server is under 300 ms when use the zip code with most children


        


        if(isset($zips[0]))// this is faster than if(count($zips)!=0)
        {//Has information

            ///////////////////
            //settlements field
            ///////////////////
            $settlements_fields = array();

            $count= count($zips);
            for($i=0; $i<$count; $i++)
            {
                $settlements_fields[]= [
                    'name' => $zips[$i]->d_asenta,
                    'zone_type' => $zips[$i]->d_zona,
                    'settlement_type'=> ["name" => $zips[$i]->d_tipo_asenta],
                ];
            }

            return response()->json([
                "zip_code" => $zip,
                "locality"=> $zips[0]->d_estado,
                "federal_entity" => [
                    "name"=> $zips[0]->d_estado,
                    "code"=> "na",
                ],
                "settlements"=>$settlements_fields,
                "municipality" => [
                    "name"=> $zips[0]->D_mnpio
                    ]
                ]);

        }
        else
        {//Not found
            return response('Not found', 404);
        }
            

        

    }


    //Faster solution with sql raw and foreach to generate the settlements field
    public function show_sql_raw_foreach($zip)
    {
        //is valid zip code?
        if(strlen($zip)!=5)
        {
            return response('Not found', 404);
        }

        $zips = \DB::select("SELECT 
        d_asenta,
        d_tipo_asenta,
        D_mnpio,
        d_estado,
        d_ciudad,
        d_zona
        FROM 
            zips 
        WHERE 
            d_codigo=? ", [$zip]); // Response in production server is under 300 ms when use the zip code with most children

        if(count($zips)!=0)
        {//Has information

            ///////////////////
            //settlements field
            ///////////////////
            $settlements_fields = array();

            foreach($zips as $key=>&$item)
            {
                
                $settlements_fields[]=[
                    'name' => $item->d_asenta,
                    'zone_type' => $item->d_zona,
                    'settlement_type'=> ["name" => $item->d_tipo_asenta],
                ];
            }


            //Template of the response 
            return response()->json(
                    [
                    "zip_code" => $zip,
                    "locality"=> $zips[0]->d_estado,
                    "federal_entity" => [
                        "name"=> $zips[0]->d_estado,
                        "code"=> "na",
                    ],
                    "settlements"=>$settlements_fields,
                    "municipality" => [
                        "name"=> $zips[0]->D_mnpio
                        ]
                    ]                
            );

        }
        else
        {//Not found
            return response('Not found', 404);
        }
            

        

    }


    //Solution with eloquent and map to generate the settlements field
    public function show_eloquent_map($zip)
    {
        //is valid zip code?
        if(strlen($zip)!=5)
        {
            return response('Not found', 404);
        }

        $zips = Zip::where('d_codigo', $zip)->get();


        if(count($zips)!=0)
        {//Has information

            ///////////////////
            //settlements field
            ///////////////////
            
            //1.- Mapping information :  This one is a little slower than the next one
            
            $settlements_fields = $zips->map(function ($zips) {
                return [
                    'name' => $zips->d_asenta,
                    'zone_type' => $zips->d_zona,
                    'settlement_type'=> ["name" => $zips->d_tipo_asenta],
                ];
                
            });
            

            //Template of the response 
            

            return response()->json(
                [
                    "zip_code" => $zip,
                    "locality"=> $zips[0]->d_estado,
                    "federal_entity" => [
                        "name"=> $zips[0]->d_estado,
                        "code"=> "na",
                    ],
                    "settlements"=>$settlements_fields,
                    "municipality" => [
                        "name"=> $zips[0]->D_mnpio
                    ]
                    ]
            );

        }
        else
        {//Not found
            return response('Not found', 404);
        }




    }

    //Solution with eloquent and foreach to generate the settlements field
    public function show_eloquent_foreach($zip)
    {
        //is valid zip code?
        if(strlen($zip)!=5)
        {
            return response('Not found', 404);
        }

        $zips = Zip::where('d_codigo', $zip)->get();


        if(count($zips)!=0)
        {//Has information

            ///////////////////
            //settlements field
            ///////////////////
            $settlements_fields=array();
            //1.- Mapping information :  This one is a little slower than the next one
            
            foreach($zips as $key=>&$item)
            {
                $settlements_fields[]=[
                    'name' => $item->d_asenta,
                    'zone_type' => $item->d_zona,
                    'settlement_type'=> ["name" => $item->d_tipo_asenta],
                ];
            }
            

            //Template of the response 
            

            return response()->json(
                [
                    "zip_code" => $zip,
                    "locality"=> $zips[0]->d_estado,
                    "federal_entity" => [
                        "name"=> $zips[0]->d_estado,
                        "code"=> "na",
                    ],
                    "settlements"=>$settlements_fields,
                    "municipality" => [
                        "name"=> $zips[0]->D_mnpio
                    ]
                    ]
            );

        }
        else
        {//Not found
            return response('Not found', 404);
        }




    }

    
}
