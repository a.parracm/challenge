<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/zip-codes/dummie', function () {//Made for testing purposes
    echo("hola");
});

Route::get('/zip-codes/{zip}', [ZipController::class, 'show_sql_raw_for']);

Route::get('/zip-codes-foreach/{zip}', [ZipController::class, 'show_sql_raw_foreach']);

Route::get('/zip-codes_eloquent/{zip}', [ZipController::class, 'show_eloquent_map']);

Route::get('/zip-codes_eloquent_foreach/{zip}', [ZipController::class, 'show_eloquent_foreach']);

