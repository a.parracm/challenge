<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zips', function (Blueprint $table) {
            $table->id();
            $table->string('d_codigo', 5);
            $table->string('d_asenta', 400);
            $table->string('d_tipo_asenta', 200);
            $table->string('D_mnpio', 200);
            $table->string('d_estado', 200);
            $table->string('d_ciudad', 200);
            $table->string('d_CP', 200);
            $table->string('c_estado', 200);
            $table->string('c_oficina', 200);
            $table->string('c_CP', 200);
            $table->string('c_tipo_asenta', 200);
            $table->string('c_mnpio', 200);
            $table->string('id_asenta_cpcons', 200);
            $table->string('d_zona', 200);
            $table->string('c_cve_ciudad', 200);

            $table->index('d_codigo');

            $table->timestamps();
        });

        \DB::statement('ALTER TABLE zips ADD FULLTEXT(d_codigo)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zips');
    }
}
